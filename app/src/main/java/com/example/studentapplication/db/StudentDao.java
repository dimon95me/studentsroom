package com.example.studentapplication.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.studentapplication.model.Student;

import java.util.List;

@Dao
public interface StudentDao {

    @Insert
    public void insertStudent(Student student);

    @Update
    public void updateStudent(Student student);

    @Delete
    public void deleteStudent(Student student);

    @Query("select * from students")
    public List<Student> getAllStudents();

    @Query("select * from students where id == :id")
    public Student getStudentById(long id);

}
