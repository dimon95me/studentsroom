package com.example.studentapplication.db;

import androidx.room.Database;

import com.example.studentapplication.model.Student;

@Database(entities = {Student.class}, version = 1)
public abstract class SqLiteDatabase {

    public abstract StudentDao getStudentDao();

}
